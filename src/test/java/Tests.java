import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.time.Duration;

public class Tests {
    WebDriver driver;
    WebDriverWait wait;
    String driverPath = "C:\\Users\\Jala\\Downloads\\geckodriver-v0.33.0-win64/geckodriver.exe";
    String baseUrl = "https://parabank.parasoft.com/parabank/admin.htm";
    @BeforeTest
    public void setUp() {
        System.out.println("Setup Process...");
        // System.setProperty("webdriver.gecko.driver", driverPath);
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get(baseUrl);
    }
    @Test
    public void checkRegisterIsWorking() {
        WebElement registerLink = driver.findElement(By.linkText("Register"));
        registerLink.click();
        Assert.assertEquals(driver.getTitle(), "ParaBank | Register for Free Online Account Access", "Register link is not clicked");
        WebElement signUpDiv = driver.findElement(By.id("rightPanel"));
        Assert.assertTrue(signUpDiv.isDisplayed());

        WebElement firstNameField = driver.findElement(By.id("customer.firstName"));
        firstNameField.sendKeys("Alice");

        WebElement lastNameField = driver.findElement(By.id("customer.lastName"));
        lastNameField.sendKeys("Doe");

        WebElement addressField = driver.findElement(By.id("customer.address.street"));
        addressField.sendKeys("H.Aliyev avenue");

        WebElement cityField = driver.findElement(By.id("customer.address.city"));
        cityField.sendKeys("Rome");

        WebElement stateField = driver.findElement(By.id("customer.address.state"));
        stateField.sendKeys("Italy");

        WebElement zipcodeField = driver.findElement(By.id("customer.address.zipCode"));
        zipcodeField.sendKeys("173292a");

        WebElement phoneField = driver.findElement(By.id("customer.phoneNumber"));
        phoneField.sendKeys("+9945785541");

        WebElement ssnField = driver.findElement(By.id("customer.ssn"));
        ssnField.sendKeys("545645321");

        WebElement usernameField = driver.findElement(By.id("customer.username"));
        usernameField.sendKeys("zhalee");

        WebElement passwordField = driver.findElement(By.id("customer.password"));
        passwordField.sendKeys("zhale123");

        WebElement confirmField = driver.findElement(By.id("repeatedPassword"));
        confirmField.sendKeys("zhale123");

        WebElement registerButton = driver.findElement(By.cssSelector("#customerForm > table > tbody > tr:nth-child(13) > td:nth-child(2) > input"));
        registerButton.click();

        WebElement successMessage = driver.findElement(By.cssSelector("#rightPanel > p"));

        Assert.assertTrue(successMessage.isDisplayed());
    }
    @Test
    public void checkForgotLoginInfo() {
         WebElement forgotLoginInfoLink = driver.findElement(By.linkText("Forgot login info?"));
         forgotLoginInfoLink.click();
         Assert.assertEquals(driver.getTitle(), "ParaBank | Customer Lookup", "You're not clicked forgot login info link");

         WebElement firstNameField = driver.findElement(By.id("firstName"));
         firstNameField.sendKeys("Alice");

         WebElement lastNameField = driver.findElement(By.id("lastName"));
         lastNameField.sendKeys("Doe");

         WebElement addressField = driver.findElement(By.id("address.street"));
         addressField.sendKeys("H.Aliyev avenue");

         WebElement cityField = driver.findElement(By.id("address.city"));
         cityField.sendKeys("Rome");

         WebElement stateField = driver.findElement(By.id("address.state"));
         stateField.sendKeys("Italy");

         WebElement zipcodeField = driver.findElement(By.id("address.zipCode"));
         zipcodeField.sendKeys("173292a");

         WebElement ssnField = driver.findElement(By.id("ssn"));
         ssnField.sendKeys("545645321");

         WebElement findMyLoginInfoButton = driver.findElement(By.cssSelector("#lookupForm > table > tbody > tr:nth-child(8) > td:nth-child(2) > input"));
         findMyLoginInfoButton.click();

        Assert.assertTrue(driver.findElement(By.cssSelector("#rightPanel > p:nth-child(3)")).getText().contains("zhalee"));
        Assert.assertTrue(driver.findElement(By.cssSelector("#rightPanel > p:nth-child(3)")).getText().contains("zhale123"));

    }
    @Test
    public void checkLoginWithInvalidCredentials() {
        WebElement usernameField = driver.findElement(By.name("username"));
        WebElement passwordField = driver.findElement(By.name("password"));
        WebElement loginButton = driver.findElement(By.xpath("//*[@id=\"loginPanel\"]/form/div[3]/input"));

        usernameField.sendKeys("Salam");
        passwordField.sendKeys("abc123#/");
        loginButton.click();

        Assert.assertEquals(driver.getTitle(), "ParaBank | Welcome | Online Banking");
    }
    @Test
    public void checkLoginWithValidCredentials() {
        WebElement usernameField = driver.findElement(By.name("username"));
        WebElement passwordField = driver.findElement(By.name("password"));
        WebElement loginButton = driver.findElement(By.xpath("//*[@id=\"loginPanel\"]/form/div[3]/input"));

        usernameField.sendKeys("zhalee");
        passwordField.sendKeys("zhale123");
        loginButton.click();

        Assert.assertEquals(driver.getTitle(), "ParaBank | Accounts Overview");
    }
    @Test
    public void openNewAccount() {
        WebElement usernameField = driver.findElement(By.name("username"));
        WebElement passwordField = driver.findElement(By.name("password"));
        WebElement loginButton = driver.findElement(By.xpath("//*[@id=\"loginPanel\"]/form/div[3]/input"));

        usernameField.sendKeys("zhalee");
        passwordField.sendKeys("zhale123");
        loginButton.click();

        WebElement openNewAccountLink = driver.findElement(By.linkText("Open New Account"));
        openNewAccountLink.click();
        WebElement openNewAccountButton = driver.findElement(By.xpath("//*[@id=\"rightPanel\"]/div/div/form/div/input"));
        openNewAccountButton.click();

        String title = driver.getTitle();
        String expectedTitle = "ParaBank | Open Account";

        Assert.assertEquals(title, expectedTitle, "New account is not opened");
    }
    @Test
    public void updateContactInfo() {
        WebElement usernameField = driver.findElement(By.name("username"));
        WebElement passwordField = driver.findElement(By.name("password"));
        WebElement loginButton = driver.findElement(By.xpath("//*[@id=\"loginPanel\"]/form/div[3]/input"));

        usernameField.sendKeys("zhalee");
        passwordField.sendKeys("zhale123");
        loginButton.click();

        WebElement updateContactInfoLink = driver.findElement(By.linkText("Update Contact Info"));
        updateContactInfoLink.click();

        WebElement firstNameField = driver.findElement(By.id("customer.firstName"));
        firstNameField.sendKeys("asdfgfdg");

        WebElement lastNameField = driver.findElement(By.id("customer.lastName"));
        lastNameField.sendKeys("sdfghgdfsgd");

        WebElement addressField = driver.findElement(By.id("customer.address.street"));
        addressField.sendKeys("sdfghfdgfs");

        WebElement cityField = driver.findElement(By.id("customer.address.city"));
        cityField.sendKeys("sdfghgdfgfh");

        WebElement stateField = driver.findElement(By.id("customer.address.state"));
        stateField.sendKeys("dgfhkjsfsg");

        WebElement zipCodeField = driver.findElement(By.id("customer.address.zipCode"));
        zipCodeField.sendKeys("jsdfhsfhsk000");

        WebElement phoneField = driver.findElement(By.id("customer.phoneNumber"));
        phoneField.sendKeys("shfshkhsvl");

        WebElement updateProfileButton = driver.findElement(By.xpath("//*[@id=\"rightPanel\"]/div/div/form/table/tbody/tr[8]/td[2]/input"));
        updateProfileButton.click();

        Assert.assertEquals(driver.getTitle(), "ParaBank | Update Profile");
    }
    @Test
    public void logOut() {
        WebElement usernameField = driver.findElement(By.name("username"));
        WebElement passwordField = driver.findElement(By.name("password"));
        WebElement loginButton = driver.findElement(By.xpath("//*[@id=\"loginPanel\"]/form/div[3]/input"));

        usernameField.sendKeys("zhalee");
        passwordField.sendKeys("zhale123");
        loginButton.click();

        WebElement logoutLink = driver.findElement(By.linkText("Log Out"));
        logoutLink.click();

        String title = driver.getTitle();

        Assert.assertEquals(title, "ParaBank | Welcome | Online Banking");
    }
    @AfterTest
    public void tearDown() {
        driver.quit();
    }
}
